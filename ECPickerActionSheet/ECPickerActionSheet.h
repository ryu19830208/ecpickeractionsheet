//
//  PickerActionSheet.h
//  PickerActionSheet
//
//  Created by SU BO-YU on 2013/11/4.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ECPickerActionSheetDelegate;

@interface ECPickerActionSheet : UIActionSheet
@property (weak, nonatomic) id<ECPickerActionSheetDelegate> pickerActionSheetDelegate;
@property (strong, nonatomic) UIToolbar *toolbar;
@property (strong, nonatomic) UIPickerView *pickerView;

- (id)initWithTitle:(NSString *)title;
- (void)showInView:(UIView *)view interfaceOrientation:(UIInterfaceOrientation)orientation;
@end

@protocol ECPickerActionSheetDelegate <NSObject>
@optional
- (void)pressDoneForPickerActionSheet:(ECPickerActionSheet *)pikcerActionSheet;
- (void)pressCancelForPickerActionSheet:(ECPickerActionSheet *)pikcerActionSheet;
@end
