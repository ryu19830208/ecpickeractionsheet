//
//  PickerActionSheet.m
//  PickerActionSheet
//
//  Created by SU BO-YU on 2013/11/4.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ECPickerActionSheet.h"

@implementation ECPickerActionSheet
@synthesize pickerActionSheetDelegate = _pickerActionSheetDelegate;
@synthesize toolbar = _toolbar;
@synthesize pickerView = _pickerView;

- (id)initWithTitle:(NSString *)title
{
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 0.0)];
    
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.toolbar.barStyle = UIBarStyleBlackOpaque;
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pressCancel:)];
    [barItems addObject:cancelBtn];
    
    UIBarButtonItem *flexSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace1];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"Avenir-Heavy" size:22];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    
    UIBarButtonItem *titleBtn = [[UIBarButtonItem alloc] initWithCustomView:titleLabel];
    [barItems addObject:titleBtn];
    
    UIBarButtonItem *flexSpace2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace2];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pressDone:)];
    [barItems addObject:doneBtn];
    
    [self.toolbar setItems:barItems animated:NO];
    
    return [self initWithTitle:title delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
}

- (void)showInView:(UIView *)view interfaceOrientation:(UIInterfaceOrientation)orientation
{
    CGRect rect = [UIScreen mainScreen].applicationFrame;
    
    CGFloat width = CGRectGetWidth(rect);
    CGFloat height = CGRectGetHeight(rect);
    if (orientation == UIDeviceOrientationLandscapeRight || orientation == UIDeviceOrientationLandscapeLeft)
    {
        width = CGRectGetHeight(rect);
        height = CGRectGetWidth(rect);
    }
    
    CGRect toolbarFrame = CGRectMake(0, 0, width, 44);
    self.toolbar.frame = toolbarFrame;
    [self.toolbar sizeToFit];
    
    CGRect pickerViewFrame = CGRectMake(0, self.toolbar.frame.size.height, 0, 0);
    self.pickerView.frame = pickerViewFrame;
    
    [self addSubview:self.toolbar];
    [self addSubview:self.pickerView];
    [self showInView:view];
    [self setBounds:CGRectMake(0, 0, width, height)];
}

- (void)pressCancel:(id)sender
{
    if (self.pickerActionSheetDelegate)
    {
        if([self.pickerActionSheetDelegate respondsToSelector:@selector(pressCancelForPickerActionSheet:)])
        {
            [self.pickerActionSheetDelegate pressCancelForPickerActionSheet:self];
        }
    }
    
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)pressDone:(id)sender
{
    if (self.pickerActionSheetDelegate)
    {
        if([self.pickerActionSheetDelegate respondsToSelector:@selector(pressDoneForPickerActionSheet:)])
        {
            [self.pickerActionSheetDelegate pressDoneForPickerActionSheet:self];
        }
    }
    
    [self dismissWithClickedButtonIndex:0 animated:YES];
}
@end
