//
//  ViewController.h
//  Demo
//
//  Created by SU BO-YU on 2013/11/4.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECPickerActionSheet.h"

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, ECPickerActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

- (IBAction)pressOpen:(id)sender;
@end
