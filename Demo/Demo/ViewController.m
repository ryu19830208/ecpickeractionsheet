//
//  ViewController.m
//  Demo
//
//  Created by SU BO-YU on 2013/11/4.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) NSMutableArray *values;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.values = [NSMutableArray array];
    for (int value = 0; value < 100; value++)
    {
        [self.values addObject:[NSNumber numberWithInt:value]];
    }
}

- (IBAction)pressOpen:(id)sender
{
    ECPickerActionSheet *pickerActionSheet = [[ECPickerActionSheet alloc] initWithTitle:@"Select Value"];
    pickerActionSheet.pickerActionSheetDelegate = self;
    
    [pickerActionSheet.pickerView setShowsSelectionIndicator:YES];
    pickerActionSheet.pickerView.dataSource = self;
    pickerActionSheet.pickerView.delegate = self;
    [pickerActionSheet showInView:self.view interfaceOrientation:self.interfaceOrientation];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.values.count;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[self.values objectAtIndex:row] stringValue];
}

#pragma mark - ECPickerActionSheetDelegate
- (void)pressDoneForPickerActionSheet:(ECPickerActionSheet *)pikcerActionSheet
{
    NSInteger row = [pikcerActionSheet.pickerView selectedRowInComponent:0];
    int value = [[self.values objectAtIndex:row] intValue];
    self.valueLabel.text = [NSString stringWithFormat:@"Select Value:%d", value];
    self.statusLabel.text = @"Select Status:Done";
}

- (void)pressCancelForPickerActionSheet:(ECPickerActionSheet *)pikcerActionSheet;
{
    self.valueLabel.text = [NSString stringWithFormat:@"Select Value:"];
    self.statusLabel.text = @"Select Status:Cancel";
}
@end
